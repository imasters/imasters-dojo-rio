import unittest


def matriz_espiral(lado):
    if lado == 1: 
        return [[1]]
    elif lado == 2:
        return [[1, 2],
                [4, 3]]
    else:
        matriz = []
        for i in range(lado):
            matriz.append([None] * lado)
        matriz[0] = range(1, lado + 1)
        matriz[1] = [8, 9, 4]
        matriz[-1] = range(7, 7-lado, -1)
        return matriz

class Test(unittest.TestCase):
    def teste_1(self):
        self.assertEqual([[1]], matriz_espiral(lado=1))

    def teste_2(self):
        self.assertEqual([[1, 2],
                          [4, 3]], matriz_espiral(lado=2))

    def teste_3(self):
        self.assertEqual([[1, 2, 3],
                          [8, 9, 4],
                          [7, 6, 5]], matriz_espiral(lado=3))

if __name__ == '__main__':
    unittest.main()
    
